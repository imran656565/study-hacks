package com.example.studyhacks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MathRules extends AppCompatActivity {
    private ListView listView;
    String s;
    String p;
    ShowResult showResult = new ShowResult();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math_rules);
        listView=findViewById(R.id.rulesList);

        String[] rules = {
                "01.   (a+b)²","02.   (a+b)²   (Corollary)", "03.   (a-b)²",
                "04.   (a-b)²   (Corollary)", "05.   a²+b²   (Corollary-1)",
                "06.   a²+b²   (Corollary-2)","07.   a²-b²", "08.   (a+b)³",
                "09.   (a+b)³   (Corollary)","10.   (a-b)³","11.   (a-b)³   (Corollary)",
                "12.   a³+b³   (Corollary)","13.   a³+b³", "14.   a³-b³   (Corollary)",
                "15.   a³-b³","16.   4ab","17.   2(a²+b²)","18.   (a+b+c)²","19.   (a²+b²+c²)",
                "20.   2(ab+bc+ca)","21.   (a+b+c)³","22.   a³+b³+c³-3abc",
                "23.   a³+b³+c³-3abc   (Corollary)","24.   (x+a)(x+b)","25.   (x+a)(x-b)",
                "26.   (x-a)(x+b)","27.   (x-a)(x-b)"};

        ArrayAdapter myAdapter = new ArrayAdapter(this, R.layout.list_view_item,R.id.itemTV, rules);

        listView.setAdapter(myAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myintent = new Intent(MathRules.this, ShowResult.class);

                switch (i)
                {
                    case 0:
                    {
                        String info1 = "(a+b)²";
                        String info2 = "(a+b) * (a+b)";
                        String info3 = "= a² + ab + ba + b²";
                        String info4 = "= a² + ab + ab + b²";
                        String info5 = "= a² + 2ab + b²";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;
                    case 1:
                    {
                        String info1 ="(a+b)² ";
                        String info2 = "= a² + 2ab + b²";
                        String info3 = "= a² + b² - 2ab + 4ab";
                        String info4 = "= a² - 2ab + b² + 4ab";
                        String info5 = "= (a-b)² + 4ab";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;
                    case 2:
                    {
                        //alt+253 is squre
                        //alt+0179 is qube

                        String info1 ="(a-b)² ";
                        String info2 = "= (a-b) * (a-b)";
                        String info3="= a² - ab - ba + b²";
                        String info4="= a² - ab - ab + b²";
                        String info5="= a² - 2ab + b²";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;
                    case 3:
                    {
                        String info1 ="(a-b)² ";
                        String info2 = "= a² - 2ab + b²";
                        String info3 = "= a² + b² + 2ab - 4ab";
                        String info4 = "= a² + 2ab + b² - 4ab";
                        String info5 = "= (a+b)² - 4ab";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;

                    case 4:
                    {
                        String info1="a²+b² = (a+b)² - 2ab";
                        String info2= "We Know,";
                        String info3= "(a+b)² = a² + 2ab + b²";
                        String info4= "=> (a+b)² - 2ab = a² + 2ab + b² - 2ab";
                        String info5= "=> (a+b)² - 2ab = a² + b²";
                        String info6= "=> a²+b² = (a+b)² - 2ab";

                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5+"\n"+info6);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;
                    case 5:
                    {
                        String info1="a²+b² = (a-b)² + 2ab";
                        String info2= "We Know,";
                        String info3= "(a-b)² = a² - 2ab + b²";
                        String info4= "=> (a-b)² + 2ab = a² - 2ab + b² + 2ab";
                        String info5= "=> (a-b)² + 2ab = a² + b²";
                        String info6= "=> a²+b² = (a-b)² + 2ab";

                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5+"\n"+info6);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;

                    case 6:
                    {
                        String info1="a²-b² ";
                        String info2= "=(a+b) (a-b)";

                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;


                    case 7:
                    {
                        String info1="(a+b)³";
                        String info2="= a³ + 3a²b + 3ab² + b³";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;
                    case 8:
                    {
                        String info1="(a+b)³";
                        String info2="= (a+b)(a+b)²";
                        String info3="= (a+b)(a²+2ab+b²)";
                        String info4="= a(a²+2ab+b²)+b(a²+2ab+b²)";
                        String info5="= a³+2a²b+ab²+(a²b+2ab²+b³)";
                        String info6="= a³+3a²b+3ab²+b³";
                        String info7="= a³+3ab(a+b)+b³";
                        String info8="= a³+b³+3ab(a+b)";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5+"\n"+info6+"\n"+info7+"\n"+info8);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;
                    case 9:
                    {
                        String info1="(a-b)³";
                        String info2="= a³ - 3a²b + 3ab² - b³";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;
                    case 10:
                    {
                        String info1="(a-b)³";
                        String info2="= (a-b) (a-b)²";
                        String info3="= (a-b) (a² - 2ab + b²)";
                        String info4="= a(a² - 2ab + b²) - b(a² - 2ab + b²)";
                        String info5="= a³ - 2a²b + ab² - a²b + 2ab² - b³";
                        String info6="= a³ - 3a²b + 3ab² - b³";
                        String info7="= a³ - b³ - 3ab(a-b)";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5+"\n"+info6+"\n"+info7);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);
                    }
                    break;
                    case 11:
                    {
                        String info1="a³ + b³";
                        String info2="= (a + b)³ - 3ab(a + b)";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;
                    case 12:
                    {
                        String info1="a³ + b³";
                        String info2="= (a + b)³ - 3ab(a + b)";
                        String info3="= (a + b) { (a + b)² - 3ab }";
                        String info4="= (a + b) (a² + 2ab + b² - 3ab)";
                        String info5="= (a + b) (a² - ab + b²)";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;
                    case 13:
                    {
                        String info1="a³ - b³";
                        String info2="= (a - b)³ + 3ab(a - b)";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;
                    case 14:
                    {
                        String info1="a³ - b³";
                        String info2="= (a - b)³ + 3ab(a - b)";
                        String info3="= (a - b) { (a - b)² + 3ab }";
                        String info4="= (a - b) (a² - 2ab + b² + 3ab)";
                        String info5="= (a - b) (a² + ab + b²)";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3+"\n"+info4+"\n"+info5);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;
                    case 15:
                    {
                        String info1="4ab";
                        String info2="= (a + b)² - (a - b)²";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;
                    case 16:
                    {
                        String info1="2(a² + b²)";
                        String info2="= (a + b)² + (a - b)²";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(false);

                    }
                    break;
                    case 17:
                    {
                        String info1="(a + b + c)²";
                        String info2="= (a² + b² + c²) + 2(ab + bc + ca)";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(false);

                    }
                    break;
                    case 18:
                    {
                        String info1="(a² + b² + c²)";
                        String info2="= (a + b + c)² - 2(ab + bc + ca)";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(false);

                    }
                    break;
                    case 19:
                    {
                        String info1="2(ab + bc + ca)²";
                        String info2="= (a + b + c)² - (a² + b² + c²)";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(false);

                    }
                    break;
                    case 20:
                    {
                        String info1="(a + b + c)³";
                        String info2="= a³+b³+c³+3(a+b)(b+c)(c+a)";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(false);

                    }
                    break;
                    case 21:
                    {
                        String info1="a³ + b³ + c³ - 3abc";
                        String info2="= (a+b+c)(a²+b²+c²-ab-bc-ca)";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(false);

                    }
                    break;
                    case 22:
                    {
                        // alt+171
                        String info1="a³ + b³ + c³ - 3abc";
                        String info2="= ½*(a+b+c){(a-b)²+(b-c)²+(c-a)²}";
                        myintent.putExtra("Imran",info1+"\n"+info2);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(false);
                    }
                    break;
                    case 23:
                    {
                        String info1="(x + a)(x + b)";
                        String info2="= x² + ax + bx + ab";
                        String info3="= x² + (a + b)x + ab";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(true);


                    }
                    break;
                    case 24:
                    {
                        String info1="(x + a)(x - b)";
                        String info2="= x² + ax - bx - ab";
                        String info3="= x² + (a - b)x - ab";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(true);


                    }
                    break;
                    case 25:
                    {
                        String info1="(x - a)(x + b)";
                        String info2="= x² + bx - ax - ab";
                        String info3="= x² + (b - a)x - ab";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(true);


                    }
                    break;
                    case 26:
                    {
                        String info1="(x - a)( x - b)";
                        String info2="= x² - ax - bx + ab";
                        String info3="= x² - (a + b)x + ab";
                        myintent.putExtra("Imran",info1+"\n"+info2+"\n"+info3);
                        showResult.setPosition(i);
                        showResult.setVisibility(true);
                        showResult.setValueForX(true);

                    }
                    break;

                }





                startActivity(myintent);



            }
        });



    }
}