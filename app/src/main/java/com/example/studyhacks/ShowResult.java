package com.example.studyhacks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ShowResult extends AppCompatActivity {
    private LinearLayout layout1,layout3;
    private RelativeLayout layout2;
    private TextView text1,text2;
    private EditText txta,txtb,txtc;
    private Button result;
    int x,y,z,res;
    int l1,l2,l3,l4,l5,l6,l7,l8,l9;

    float f1;
    private static int i=0;
    private static boolean statusForThree = false;
    private static boolean statusForX = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);

        layout1= (LinearLayout) findViewById(R.id.layout1);
        layout3= (LinearLayout) findViewById(R.id.layout3);

        layout2= (RelativeLayout) findViewById(R.id.layout2);

        text1= (TextView) findViewById(R.id.txt1);
        text2= (TextView) findViewById(R.id.txt2);
        txta= (EditText) findViewById(R.id.a);
        txtb= (EditText) findViewById(R.id.b);
        txtc= (EditText) findViewById(R.id.c);
        result= (Button) findViewById(R.id.btnResult);

        if(statusForThree){
            txtc.setVisibility(View.VISIBLE);
        }

        if (statusForX){
            txtc.setHint("Value for x");
        }


        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (i) {
                    case 0:{
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            String mess="Enter value of a and b";
                            text2.setText("0");
                            Toast.makeText(getApplicationContext(),mess,Toast.LENGTH_SHORT).show();
                        }
                        else{
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x*x;
                            l2=2*x*y;
                            l3=y*y;
                            res = ((x * x) + 2 * x * y + (y * y));
                            // text2.setText("\n"+res);
                            String line1="("+x+" + "+y+")²";
                            String line2="\n= "+x+"² + "+"2."+x+"."+y+" + "+y+"²";
                            String line3="\n= "+l1+" + "+l2+" + "+l3;
                            String line4="\n= "+res;
                            text2.setText(line1+line2+line3+line4);

                        }


                    }
                    break;
                    case 1:{
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x-y;
                            l2=4*x*y;
                            l3=l1*l1;
                            res = ((x - y) * (x - y) + 4 * x * y);
                            //text2.setText("" + res);
                            String line1="("+x+" + "+y+")²";
                            String line2="\n= ("+x+" - "+y+")² + 4."+x+"."+y;
                            String line3="\n= "+l1+"² + "+l2;
                            String line4="\n= "+l3+" + "+l2;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }
                    }
                    break;
                    case 2: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x*x;
                            l2=2*x*y;
                            l3=y*y;
                            res = ((x * x) - 2 * x * y + (y * y));
                            //text2.setText("" + res);
                            String line1="("+x+" - "+y+")²";
                            String line2="\n= "+x+"² - "+"2."+x+"."+y+" + "+y+"²";
                            String line3="\n= "+l1+" - "+l2+" + "+l3;
                            String line4="\n= "+res;
                            text2.setText(line1+line2+line3+line4);
                        }

                    }
                    break;
                    case 3:{
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x+y;
                            l2=4*x*y;
                            l3=l1*l1;
                            res = ((x + y) * (x + y ) - 4*x*y);
                            //text2.setText("" + res);
                            String line1="("+x+" - "+y+")²";
                            String line2="\n= ("+x+" + "+y+")² - 4."+x+"."+y;
                            String line3="\n= "+l1+"² - "+l2;
                            String line4="\n= "+l3+" - "+l2;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }
                    }
                    break;
                    case 4: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x+y;
                            l2=2*x*y;
                            l3=l1*l1;
                            res = ((x + y) * (x + y ) - 2*x*y);
                            //text2.setText("" + res);
                            String line1= +x+"² + "+y+"²";
                            String line2="\n= ("+x+" + "+y+")² - 2."+x+"."+y;
                            String line3="\n= ("+l1+")² - "+l2;
                            String line4="\n= "+l3+" - "+l2;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }


                    }
                    break;
                    case 5: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x-y;
                            l2=2*x*y;
                            l3=l1*l1;
                            res = ((x - y) * (x - y ) + 2*x*y);
                            //text2.setText("" + res);
                            String line1= +x+"² + "+y+"²";
                            String line2="\n= ("+x+" - "+y+")² + 2."+x+"."+y;
                            String line3="\n= ("+l1+")² + "+l2;
                            String line4="\n= "+l3+" + "+l2;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }
                    }
                    break;
                    case 6: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x+y;
                            l2=x-y;
                            res = ((x + y) * (x - y ));
                            //text2.setText("" + res);
                            String line1="("+x+")² - ("+y+")²";
                            String line2="\n= ("+x+" + "+y+")("+x+" - "+y+")";
                            String line3="\n= "+l1+" * "+l2;
                            String line4="\n= "+res;
                            text2.setText(line1+line2+line3+line4);
                        }

                    }
                    break;
                    case 7: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x*x*x;
                            l2=x*x;
                            l3=y*y;
                            l4=y*y*y;
                            l5=3*l2*y;
                            l6=3*x*l3;
                            res = ((x*x*x)+3*x*x*y+3*x*y*y+(y*y*y));
                            //text2.setText("" + res);
                            String line1="("+x+" + "+y+")³";
                            String line2="\n= "+x+"³ + 3."+x+"²."+y+" + 3."+x+"."+y+"² + "+y+"³";
                            String line3="\n= "+l1+" + 3."+l2+"."+y+" + 3."+x+"."+l3+" + "+l4;
                            String line4="\n= "+l1+" + "+l5+" + "+l6+" + "+l4;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 8: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x*x*x;
                            l2=y*y*y;
                            l3=3*x*y;
                            l4=x+y;
                            l5=l3*l4;
                            res = ((x*x*x)+(y*y*y)+3*x*y*(x+y));
                            //text2.setText("" + res);
                            String line1="("+x+" + "+y+")³";
                            String line2="\n= "+x+"³ + "+y+"³ + 3."+x+"."+y+". ("+x+" + "+y+")";
                            String line3="\n= "+l1+" + "+l2+" + "+l3+"."+l4;
                            String line4="\n= "+l1+" + "+l2+" +"+l5;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 9: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x*x*x;
                            l2=x*x;
                            l3=y*y;
                            l4=y*y*y;
                            l5=3*l2*y;
                            l6=3*x*l3;
                            res = ((x*x*x)-3*x*x*y+3*x*y*y-(y*y*y));
                            //text2.setText("" + res);
                            String line1="("+x+" - "+y+")³";
                            String line2="\n= "+x+"³ - 3."+x+"²."+y+" + 3."+x+"."+y+"² - "+y+"³";
                            String line3="\n= "+l1+" - 3."+l2+"."+y+" + 3."+x+"."+l3+" - "+l4;
                            String line4="\n= "+l1+" - "+l5+" + "+l6+" - "+l4;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 10: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x*x*x;
                            l2=y*y*y;
                            l3=3*x*y;
                            l4=x-y;
                            l5=l3*l4;
                            res = ((x*x*x)-(y*y*y)-3*x*y*(x-y));
                            //text2.setText("" + res);
                            String line1="("+x+" - "+y+")³";
                            String line2="\n= "+x+"³ - "+y+"³ - 3."+x+"."+y+". ("+x+" - "+y+")";
                            String line3="\n= "+l1+" - "+l2+" - "+l3+"."+l4;
                            String line4="\n= "+l1+" - "+l2+" -"+l5;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 11: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x+y;
                            l2=3*x*y;
                            l3=l1*l1*l1;
                            l4=l2*l1;
                            res = ((x + y)*(x + y )*(x + y)- 3*x*y*(x+y));
                            //text2.setText("" + res);
                            String line1="("+x+")³ + ("+y+")³";
                            String line2="\n= ("+x+" + "+y+")³ - 3."+x+"."+y+". ("+x+" + "+y+")";
                            String line3="\n= ("+l1+")³ - "+l2+"."+l1;
                            String line4="\n= "+l3+" - "+l4;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }


                    }
                    break;
                    case 12: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x*y;
                            l2=x*x;
                            l3=x*y;
                            l4=y*y;
                            l5=l2-l3+l4;
                            res = ((x + y)*(x*x-x*y+y*y));
                            //text2.setText("" + res);
                            String line1="("+x+")³ + ("+y+")³";
                            String line2="\n= ("+x+" + "+y+")("+x+"² - "+x+"."+y+" + "+y+"²)";
                            String line3="\n= "+l1+" . ("+l2+" - "+l3+" + "+l4+")";
                            String line4="\n= "+l1+" . "+l5;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 13: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x-y;
                            l2=3*x*y;
                            l3=l1*l1*l1;
                            l4=l2*l1;
                            res = ((x - y)*(x - y )*(x - y)+3*x*y*(x-y));
                            //text2.setText("" + res);
                            String line1="("+x+")³ - ("+y+")³";
                            String line2="\n= ("+x+" - "+y+")³ + 3."+x+"."+y+". ("+x+" - "+y+")";
                            String line3="\n= ("+l1+")³ + "+l2+"."+l1;
                            String line4="\n= "+l3+" + "+l4;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 14: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x-y;
                            l2=x*x;
                            l3=x*y;
                            l4=y*y;
                            l5=l2+l3+l4;
                            res = ((x - y)*(x*x+x*y+y*y));
                            //text2.setText("" + res);
                            String line1="("+x+")³ - ("+y+")³";
                            String line2="\n= ("+x+" - "+y+")("+x+"² + "+x+"."+y+" + "+y+"²)";
                            String line3="\n= "+l1+" . ("+l2+" + "+l3+" + "+l4+")";
                            String line4="\n= "+l1+" . "+l5;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 15: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x+y;
                            l2=x-y;
                            l3=l1*l1;
                            l4=l2*l2;
                            res = ((x+y)*(x+y)-(x-y)*(x-y));
                            //text2.setText("" + res);
                            String line1="4."+x+"."+y;
                            String line2="\n= ("+x+" + "+y+")² - ("+x+" - "+y+")²";
                            String line3="\n= ("+l1+")² - ("+l2+")²";
                            String line4="\n= "+l3+" - "+l4;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 16: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0) {
                            messfor2();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            l1=x+y;
                            l2=x-y;
                            l3=l1*l1;
                            l4=l2*l2;
                            res = ((x+y)*(x+y)+(x-y)*(x-y));
                            //text2.setText("" + res);
                            String line1="2("+x+"² + "+y+"²)";
                            String line2="\n= ("+x+" + "+y+")² + ("+x+" - "+y+")²";
                            String line3="\n= ("+l1+")² + ("+l2+")²";
                            String line4="\n= "+l3+" + "+l4;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;

                    case 17: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=x*x;
                            l2=y*y;
                            l3=z*z;
                            l4=x*y;
                            l5=y*z;
                            l6=z*x;
                            l7=l1+l2+l3;
                            l8=l4+l5+l6;
                            l9=2*l8;
                            res = ((x*x+y*y+z*z)+2*(x*y+y*z+z*x));
                            //text2.setText("" + res);
                            String line1="("+x+" + "+y+" + "+z+")²";
                            String line2="\n= ("+x+"² + "+y+"² + "+z+"²) + 2("+x+"."+y+" + "+y+"."+z+" + "+z+"."+x+")";
                            String line3="\n= ("+l1+" + "+l2+" + "+l3+") + 2("+l4+" + "+l5+" + "+l6+")";
                            String line4="\n= "+l7+" + 2 . "+l8;
                            String line5="\n= "+l7+" + "+l9;
                            String line6="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5+line6);
                        }
                    }
                    break;
                    case 18: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=x+y+z;
                            l2=x*y;
                            l3=y*z;
                            l4=z*x;
                            l5=l1*l1;
                            l6=l2+l3+l4;
                            l7=2*l6;
                            res = ((x+y+z)*(x+y+z)-2*(x*y+y*z+z*x));
                            //text2.setText("" + res);
                            String line1=x+"² + "+y+"² + "+z+"²";
                            String line2="\n= ("+x+" + "+y+" + "+z+")² - 2("+x+"."+y+" + "+y+"."+z+" + "+z+"."+x+")";
                            String line3="\n= ("+l1+")² - 2("+l2+" + "+l3+" + "+l4+")";
                            String line4="\n= "+l5+" - 2 . "+l6;
                            String line5="\n= "+l5+" - "+l7;
                            String line6="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5+line6);
                        }
                    }
                    break;
                    case 19: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=x+y+z;
                            l2=x*x;
                            l3=y*y;
                            l4=z*z;
                            l5=l1*l1;
                            l6=l2+l3+l4;
                            res = ((x+y+z)*(x+y+z)-(x*x+y*y+z*z));
                            //text2.setText("" + res);
                            String line1="2("+x+"."+y+" + "+y+"."+z+" + "+z+"."+x+")²";
                            String line2="\n= ("+x+" + "+y+" + "+z+")² - ("+x+"² + "+y+"² + "+z+"²)";
                            String line3="\n= ("+l1+")² - ("+l2+" + "+l3+" + "+l4+")";
                            String line4="\n= "+l5+" - "+l6;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 20: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=x*x*x;
                            l2=y*y*y;
                            l3=z*z*z;
                            l4=x+y;
                            l5=y+z;
                            l6=z+x;
                            l7=l1+l2+l3;
                            l8=3*l4*l5*l6;
                            res = ((x*x*x+y*y*y+z*z*z)+3*(x+y)*(y+z)*(z+x));
                            //text2.setText("" + res);
                            String line1="("+x+" + "+y+" + "+z+")³";
                            String line2="\n= "+x+"³ + "+y+"³ + "+z+"³ + 3("+x+" + "+y+")("+y+" + "+z+" )("+z+" + "+x+")";
                            String line3="\n= "+l1+" + "+l2+" + "+l3+" + 3."+l4+"."+l5+"."+l6;
                            String line4="\n= "+l7+" + "+l8;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }
                    }
                    break;
                    case 21: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3();
                        }else{
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=x+y+z;
                            l2=x*x;
                            l3=y*y;
                            l4=z*z;
                            l5=x*y;
                            l6=y*z;
                            l7=z*x;
                            l8=(l2+l3+l4)-(l5+l6+l7);

                            res = ((x+y+z)*(x*x+y*y+z*z-x*y-y*z-z*x));
                            //text2.setText("" + res);
                            String line1="("+x+"³ + "+y+"³ + "+z+"³) - 3."+x+"."+y+"."+z;
                            String line2="\n= ("+x+"+"+y+"+"+z+")("+x+"²+"+y+"²+"+z+"²-"+x+"."+y+"-"+y+"."+z+"-"+z+"."+x+")";
                            String line3="\n= "+l1+" * ("+l2+" + "+l3+" + "+l4+" - "+l5+" - "+l6+" - "+l7+")";
                            String line4="\n= "+l1+" * "+l8;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);

                        }

                    }
                    break;
                    case 22: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3();
                        }else{
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=x+y+z;
                            l2=x-y;
                            l3=y-z;
                            l4=z-x;
                            f1= (float) (.5*l1);
                            l6=l2*l2;
                            l7=l3*l3;
                            l8=l4*l4;
                            l9=l6+l7+l8;
                            res = (int) ((0.5*(x+y+z))*((x-y)*(x-y)+(y-z)*(y-z)+(z-x)*(z-x)));
                            //text2.setText("" + res);
                            String line1="("+x+"³ + "+y+"³ + "+z+"³) - 3."+x+"."+y+"."+z;
                            String line2="\n= ½ *("+x+"+"+y+"+"+z+"){("+x+"-"+y+")² + ("+y+"-"+z+")² + ("+z+"-"+x+")²}";
                            String line3="\n= ½ * "+l1+" {("+l2+")² + ("+l3+")² + ("+l4+")²}";
                            String line4="\n= "+f1+" * ("+l6+" + "+l7+" + "+l8+")";
                            String line5="\n= "+f1+" * "+l9;
                            String line6="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5+line6);
                        }

                    }
                    break;
                    case 23: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3x();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=z*z;
                            l2=x+y;
                            l3=x*y;
                            l4=l2*z;
                            res = (z*z+(x+y)*z+x*y);
                            //text2.setText("" + res);
                            String line1="("+z+" + "+x+")("+z+" + "+y+")";
                            String line2="\n= "+z+"² + ("+x+" + "+y+")*"+z+" + "+x+"."+y;
                            String line3="\n= "+l1+" + "+l2+"*"+z+" + "+l3;
                            String line4="\n= "+l1+" + "+l4+" + "+l3;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);
                        }

                    }
                    break;
                    case 24: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3x();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=z*z;
                            l2=x-y;
                            l3=x*y;
                            l4=l2*z;
                            res = (z*z+(x-y)*z-x*y);
                            //text2.setText("" + res);
                            String line1="("+z+" + "+x+")("+z+" - "+y+")";
                            String line2="\n= "+z+"² + ("+x+" - "+y+")*"+z+" - "+x+"."+y;
                            String line3="\n= "+l1+" + "+l2+"*"+z+" - "+l3;
                            String line4="\n= "+l1+" + "+l4+" - "+l3;
                            String line5="\n= "+res;
                            String line6="\n= "+l1+" + ("+l2+"*"+z+") - "+l3;
                            String line7="\n= "+l1+" "+l4+" - "+l3;
                            if (x>y){
                                text2.setText(line1+line2+line3+line4+line5);
                            }else if (x==y){
                                text2.setText(line1+line2+line3+line4+line5);
                            }else if (x<y){
                                text2.setText(line1+line2+line6+line7+line5);
                            }
                        }

                    }
                    break;
                    case 25: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3x();
                        }else{
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=z*z;
                            l2=y-x;
                            l3=x*y;
                            l4=l2*z;
                            res = (z*z+(y-x)*z-x*y);
                            //text2.setText("" + res);
                            String line1="("+z+" - "+x+")("+z+" + "+y+")";
                            String line2="\n= "+z+"² + ("+y+" - "+x+")*"+z+" - "+x+"."+y;
                            String line3="\n= "+l1+" + "+l2+"*"+z+" - "+l3;
                            String line4="\n= "+l1+" + "+l4+" - "+l3;
                            String line5="\n= "+res;
                            String line6="\n= "+l1+" + ("+l2+"*"+z+") - "+l3;
                            String line7="\n= "+l1+" "+l4+" - "+l3;
                            if (x<y){
                                text2.setText(line1+line2+line3+line4+line5);
                            }else if (x==y){
                                text2.setText(line1+line2+line3+line4+line5);
                            }else if (x>y){
                                text2.setText(line1+line2+line6+line7+line5);
                            }

                        }

                    }
                    break;
                    case 26: {
                        if (txta.getText().length()==0 || txtb.getText().length()==0 || txtc.getText().length()==0) {
                            messfor3x();
                        }else {
                            text2.setText("");
                            x = Integer.parseInt(txta.getText().toString());
                            y = Integer.parseInt(txtb.getText().toString());
                            z = Integer.parseInt(txtc.getText().toString());
                            l1=z*z;
                            l2=y+x;
                            l3=x*y;
                            l4=l2*z;
                            res = (z*z-(x+y)*z+x*y);
                            //text2.setText("\n"+ res);
                            String line1="("+z+" - "+x+")("+z+" - "+y+")";
                            String line2="\n= "+z+"² - ("+x+" + "+y+")*"+z+" + "+x+"."+y;
                            String line3="\n= "+l1+" - "+l2+"*"+z+" + "+l3;
                            String line4="\n= "+l1+" - "+l4+" + "+l3;
                            String line5="\n= "+res;
                            text2.setText(line1+line2+line3+line4+line5);

                        }
                    }
                    break;

                }





            }
        });



        Intent t=getIntent();
        String in=t.getStringExtra("Imran");
//        String s = t.getStringExtra("position");
//
//        i = Integer.parseInt(s);
//        Log.d("Position",i+"");

        text1.setText(in);


    }

    public void setVisibility(boolean x){
        this.statusForThree = x;
    }

    public void setValueForX(boolean x){
        this.statusForX = x;
    }

    public void setPosition(int position){
        /*if(position==1){
            text1.setTextSize(10);
        }*/
        this.i = position;
    }
    /*
    public void messge2(){
        if (txta.getText().length()==0 || txtb.getText().length()==0){
            String mess="Enter value of a and b";
            Toast.makeText(getApplicationContext(),mess,Toast.LENGTH_SHORT).show();

        }
        else if (txta.getText().length()==1 || txtb.getText().equals("")){
            String mess="Enter value of a";
            Toast.makeText(getApplicationContext(),mess,Toast.LENGTH_SHORT).show();
        }
        else if (txta.getText().length()==1 || txtb.getText().length()==0){
            String mess="Enter value of b ";
            Toast.makeText(getApplicationContext(),mess,Toast.LENGTH_SHORT).show();
        }
    }*/
    public void messfor2(){
        String mess="Enter value of a and b";
        text2.setText("0");
        Toast.makeText(getApplicationContext(),mess,Toast.LENGTH_SHORT).show();
    }
    public void messfor3(){
        String mess="Enter value of a,b and c";
        text2.setText("0");
        Toast.makeText(getApplicationContext(),mess,Toast.LENGTH_SHORT).show();
    }
    public void messfor3x(){
        String mess="Enter value of a,b and x";
        text2.setText("0");
        Toast.makeText(getApplicationContext(),mess,Toast.LENGTH_SHORT).show();
    }


}