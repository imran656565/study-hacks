package com.example.studyhacks;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class Convert extends AppCompatActivity {
    private EditText input, output;
    private Button convert;
    private RadioButton couvert_into_dollar, convert_into_taka;
    private String amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convert);

        init();


        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount = input.getText().toString();

                if (amount.equals("")) {
                    Toast.makeText(Convert.this, "Enter amount", Toast.LENGTH_SHORT).show();

                } else {

                    if (couvert_into_dollar.isChecked()) {

                        float totalamount =Float.parseFloat(amount);
                        float result = totalamount / 84;
                        output.setText(String.valueOf(result));

                    } else if (convert_into_taka.isChecked()) {


                        float totalamount = Float.parseFloat(amount);
                        float result = totalamount * 84;
                        output.setText(String.valueOf(result));
                    } else {
                        Toast.makeText(Convert.this, "Select any option", Toast.LENGTH_SHORT).show();


                    }

                }


            }
        });


    }

    private void init() {
        input = findViewById(R.id.c_input);
        output = findViewById(R.id.c_output);
        couvert_into_dollar = findViewById(R.id.convert_into_dollar);
        convert_into_taka = findViewById(R.id.convert_into_taka);
        convert = findViewById(R.id.c_button);
    }

}