package com.example.studyhacks;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class Chemistry extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chemistry);
        Toasty.normal(this,"Turn on rotation for better visual.",Toasty.LENGTH_SHORT).show();
    }

}